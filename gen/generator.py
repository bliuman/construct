import command
import os

class Base:

	cmd_factory = None
	
	def __init__(self, factory):
		self.cmd_factory = factory
		
	def gen_setup_options(self):
		return None
				
	def gen_setup_step(self, cmds, options):
		raise NotImplementedError()
		
	def gen_create_project_options(self):
		return None
		
	def gen_create_project_step(self, cmds, options, cmake_parameters):
		raise NotImplementedError()
		
	def gen_build_project_options(self):
		return None
		
	def gen_build_project_step(self, cmds, options):
		raise NotImplementedError()
		
	def gen_cleanup_options(self):
		return None
		
	def gen_cleanup_step(self, cmds, options):
		raise NotImplementedError()
		

class Common(Base):
	
	def __init__(self, factory):
		Base.__init__(self, factory)
		
	def gen_setup_step(self, cmds, options):
		cmds.append(self.cmd_factory.remove_dir("build"))
		cmds.append(self.cmd_factory.remove_dir("lib"))
		cmds.append(self.cmd_factory.remove_dir("libs"))
		cmds.append(self.cmd_factory.remove_dir("bin"))
		cmds.append(self.cmd_factory.make_dir("build"))
		cmds.append(self.cmd_factory.change_dir("build"))
		
		
class MacOSX(Common):
	
	def __init__(self, factory):
		Common.__init__(self, factory)
		
	def gen_create_project_step(self, cmds, options, cmake_parameters):
		arguments = ["-GXcode"]
		if (cmake_parameters):
			arguments.append(cmake_parameters)
		cmds.append(self.cmd_factory.run_cmake("..", arguments))
		
	def gen_build_project_step(self, cmds, options):
		args = [
			"xcodebuild", 
			"-configuration", 
			options["configuration"], 
			"-target", 
			options["target"]
		]
		cmds.append(' '.join(args))
		
	
class iOS(Common):
	
	def __init__(self, factory):
		Common.__init__(self, factory)
		
	def gen_create_project_step(self, cmds, options, cmake_parameters):
		arguments = [
			"-DCMAKE_TOOLCHAIN_FILE=" + os.environ["IOS_TOOLCHAIN_FILE"],
			"-DIOS_PLATFORM=" + options["ios_platform"],
			"-GXcode"
		],
		if (cmake_parameters):
			arguments.append(cmake_parameters)
		cmds.append(self.cmd_factory.run_cmake("..", arguments))
		
	def gen_build_project_step(self, cmds, options):
		args = [
			"xcodebuild",
			"ARCHS=" + options["ios_archs"],
			"ONLY_ACTIVE_ARCH=NO",
			"-configuration",
			options["configuration"],
			"-target",
			options["target"]
		]
		cmds.append(' '.join(args))
		
		
class WindowsAndroid(Common):

	def __init__(self, factory):
		Common.__init__(self, factory)
		
	def gen_create_project_step(self, cmds, options, cmake_parameters):
		args = [
			"-G\"MinGW Makefiles\"",
			"-DCMAKE_TOOLCHAIN_FILE=" + os.environ["ANDROID_TOOLCHAIN_FILE"],
			"-DCMAKE_MAKE_PROGRAM=" + os.environ["ANDROID_MAKE_PROGRAM"],
			"-DANDROID_ABI=" + options["android_abi"],
			"ANDROID_STL=gnustl_static"
		]
		if cmake_parameters:
			args.append(cmake_parameters)
		cmds.append(self.cmd_factory.run_cmake("..", args))
		
	def gen_build_project_step(self, cmds, options):
		args = [
			"cmake",
			"--build",
			"."
		]
		cmds.append(" ".join(args))
		
		
class UnixAndroid(Common):
	
	def __init__(self, factory):
		Common.__init__(self, factory)
		
	def gen_create_project_step(self, cmds, options, cmake_parameters):
		args = [
			"-DCMAKE_TOOLCHAIN_FILE=" + os.environ["ANDROID_TOOLCHAIN_FILE"],
			"-DANDROID_ABI=" + options["android_abi"]
		]
		if cmake_parameters:
			args.append(cmake_parameters)
		cmds.append(self.cmd_factory.run_cmake("..", args))
		
	def gen_build_project_step(self, cmds, options):
		args = [
			"make",
			"-j8"
		]
		cmds.append(" ".join(args))
		
		
class Windows(Common):

	def __init__(self, factory):
		Common.__init__(self, factory)
		
	def gen_create_project_step(self, cmds, options, cmake_parameters):
		cmds.append(self.cmd_factory.run_cmake("..", cmake_parameters))
		
	def gen_build_project_step(self, cmds, options):
		args = [
			"devenv",
			"/build",
			options["configuration"],
			"/project",
			"ALL_BUILD",
			options["win_project_name"] + ".sln"
		]
		cmds.append(self.cmd_factory.change_dir("build"))
		cmds.append(" ".join(args))
		cmds.append(self.cmd_factory.up_one_dir())
		
		
class Linux(Common):

	def __init__(self, factory):
		Common.__init(self, factory)
		

def is_valid_target_platform(host, target):
	valid_map = {
		"Darwin": ("MacOSX", "iOS", "Android"),
		"Windows": ("Windows", "Android"),
		"Linux": ("Linux", "Android")
	}
	supported = valid_map[host]
	if not supported or not target in supported:
		return False
	else:
		return True
		
def create_generator(host, target):
	if not is_valid_target_platform(host, target):
		raise Exception("Target platform not supported")
		
	cmd_factory = command.create_command_factory(host)
	if ("MacOSX" == target):
		return MacOSX(cmd_factory)
	elif ("iOS" == target):
		return iOS(cmd_factory)
	elif ("Android" == target):
		if ("Windows" == host):
			return WindowsAndroid(cmd_factory)
		else:
			return UnixAndroid(cmd_factory)
	elif ("Windows" == target):
		return Windows(cmd_factory)
	elif ("Linux" == target):
		return Linux(cmd_factory)
	else:
		raise Exception("Target platform not supported")