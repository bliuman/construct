class CommandFactory:
	
	dir_postfix = None
	
	def __init__(self, dir_postfix):
		self.dir_postfix = dir_postfix
		
	def change_dir(self, dir):
		return "cd " + self._dir(dir)
		
	def up_one_dir(self):
		return self.change_dir("..")
		
	def remove_dir(self, dir):
		raise NotImplmentedError()
		
	def make_dir(self, dir):
		return "mkdir " + self._dir(dir)
		
	def run_cmake(self, dir, options):
		if not options:
			options = ["cmake"]
		else:
			options.insert(0, "cmake")
		options.append(self._dir(dir))
		return " ".join(options)
		
	def _dir(self, dir):
		if self.dir_postfix:
			return dir + self.dir_postfix
		else:
			return dir
					
		
class WindowsCmdFactory(CommandFactory):

	def __init__(self):
		CommandFactory.__init__(self, None)
		
	def remove_dir(self, dir):
		return "rmdir /s /q " + self._dir(dir)
			
		
class UnixCmdFactory(CommandFactory):
	
	def __init__(self):
		CommandFactory.__init__(self, "/")
		
	def remove_dir(self, dir):
		return "rm -r -f " + self._dir(dir)
		
		
def create_command_factory(host_system):
	if ("Windows" == host_system):
		return WindowsCmdFactory()
	else:
		return UnixCmdFactory()