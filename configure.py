if __name__ == "__main__":
	from optparse import OptionParser
	parser = OptionParser()
	parser.add_option("-t", "--target", dest="target", help="build target platform")
	parser.add_option("-p", "--project", dest="project", help="visual studio solution name (windows only)")
	parser.add_option("--nobuild", dest="nobuild", action="store_true", help="only generate build project")
	parser.add_option("--cmake_params", dest="cmake_params", help="command line parameters for cmake executable")
	(options, args) = parser.parse_args()
	from gen import generator
	import platform
	g = generator.create_generator(platform.system(), options.target)
	cmds = []
	g.gen_setup_step(cmds, {})
	if options.cmake_params:
		cmake_params = options.cmake_params
	else:
		cmake_params = None
	g.gen_create_project_step(cmds, {"ios_platform": "OS", "android_abi": "armeabi-v7a"}, cmake_params)
	if options.project:
		proj_name = options.project
	else:
		proj_name = ""
	
	if not options.nobuild:
		g.gen_build_project_step(cmds, {"configuration": "Debug", "target": "ALL_BUILD", "ios_archs": "\"i386 armv7 armv7s\"", "win_project_name": proj_name})
	
	import os
	if "Windows" == platform.system():
		file_ext = ".bat"
	else:
		file_ext = ".sh"
	file_path = os.path.join(os.getcwd(), "build_script") + file_ext
	print file_path
	file = open(file_path, "w")
	for cmd in cmds:
		file.write(cmd + "\n")
	file.close()
	import subprocess
	if "Windows" == platform.system():
		subprocess.call(["cmd", "/c", file_path])
	else:
		subprocess.call(["chmod", "+x", file_path])
		subprocess.call(["/bin/sh", file_path])